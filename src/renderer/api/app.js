import request from '@/utils/request'
export function get_ad(token) {
  return   request({
    url: '/app/ad',
    method: 'get',
    params: { token }
  });
}


export function get_article(token) {
  return request({
    url: '/app/article',
    method: 'get',
    params: { token }
  })
}


export function get_video(token) {
  return request({
    url: '/app/video',
    method: 'get',
    params: { token }
  })
}
