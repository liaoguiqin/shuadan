import request from '@/utils/request'
const path = {
  'app_flow':'task/add', // app流量
  'collect':'task/add', //搜索收藏
  'cart':'task/add', //商品收藏
  'shop':'task/add', //店铺关注
  'jg_collect':'task/add', //搜索加购
  'jg_cart':'task/add', //商品加购
  'jd_zb': "task/add", //直播
}
export function app_flow(param) {
    console.log("111")
   return  request({
      url:path.app_flow,
      method: 'post',
      data: {
        ...param
      }
    })




}
export function collect(param) {
  return request({
    url:path.collect,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function cart(param) {
  return request({
    url:path.cart,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function shop(param) {
  return request({
    url:path.shop,
    method: 'post',
    data: {
      ...param
    }
  })
}export function jg_cart(param) {
  return request({
    url:path.jg_cart,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function jg_collect(param) {
  return request({
    url:path.jg_collect,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function jd_zb(param) {
  return request({
    url:path.jd_zb,
    method: 'post',
    data: {
      ...param
    }
  })
}

