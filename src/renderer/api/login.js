import request from '@/utils/request'

export function login(account, password) {
  return request({
    url: '/user/login',
    method: 'post',
    data: {
      account,
      password
    }
  })
}

export function getInfo(token) {
  return request({
    url: '/app/getinfo',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/user/logout',
    method: 'post'
  })
}

export function from() {
  return request({
    url: '/user/login',
    method: 'post',
  })
}

export function find_pass(param) {
  return request({
    url: 'user/find_pass',
    method: 'post',
    data:{
      ...param
    }
  })
}
export function btn_code(param) {
  return request({
    url: 'user/send_code',
    method: 'post',
    data:{
      ...param
    }
  })
}
export function register(param) {
  return request({
    url: 'user/register',
    method: 'post',
    data:{
      ...param
    }
  })
}
