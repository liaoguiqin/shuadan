import request from '@/utils/request'
const path = {
  'pay':'pay/alipay', // 支付
}

export function  pay(param) {
  console.log(param)
  return request({
    url:path.pay,
    method: 'post',
    data: {
      ...param

    }
  })
}
