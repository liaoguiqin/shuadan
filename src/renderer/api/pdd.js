import request from '@/utils/request'
const path = {
  'app_flow':'task/add', // app流量
  'collect':'task/add', //搜索收藏
  'cart':'task/add', //商品收藏
  'shop':'task/add', //店铺收藏
}
export function  app_flow(param) {
  for (let i = 0; i < 2; i++) {

    return request({
      url:path.app_flow,
      method: 'post',
      data: {
        ...param
      }
    })
  }

}
export function collect(param) {
  return request({
    url:path.collect,
    method: 'post',
    data: {
      ...param
    }
  })
}

export function cart(param) {
  return request({
    url:path.cart,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function shop(param) {
  return request({
    url:path.shop,
    method: 'post',
    data: {
      ...param
    }
  })
}
