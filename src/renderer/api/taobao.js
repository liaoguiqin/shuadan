import request from '@/utils/request'
const path = {
  'app_flow':'task/add', // app流量
  'like':'task/add', // 宝贝点赞
  'pc_flow':'task/add', // pc流量
  'may_like':'task/add', //猜你喜欢
  'fine_shop':'task/add', //每日好店
  'Panning_password':'task/add', //淘口令
  'collect':'task/add', //搜索收藏
  'cart':'task/add', //商品收藏
  'shop':'task/add', //店铺收藏
  'jg_collect':'task/add', //搜索加购
  'jg_cart':'task/add', //商品加购
  'tb_zb': "task/add", //直播
  'readpc_flow':'task/add', // pc流量
  'read_like':'task/add', // 宝贝点赞
  'readmay_like':'task/add', //猜你喜欢

}
export function  app_flow(param) {
  console.log(param,'11')
  return  request({
    url:path.app_flow,
    method: 'post',
    data: {
      ...param
    }
  })

}
export function like(param) {
  return request({
    url:path.like,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function pc_flow(param) {
  return request({
    url:path.pc_flow,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function may_like(param) {
  return request({
    url:path.may_like,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function fine_shop(param) {
  return request({
    url:path.fine_shop,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function Panning_password(param) {
  return request({
    url:path.Panning_password,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function collect(param) {
  return request({
    url:path.collect,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function cart(param) {
  return request({
    url:path.cart,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function shop(param) {
  return request({
    url:path.shop,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function jg_collect(param) {
  return request({
    url:path.jg_collect,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function jg_cart(param) {
  return request({
    url:path.jg_cart,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function tb_zb(param) {
  return request({
    url:path.tb_zb,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function readpc_flow(param) {
  return request({
    url:path.readpc_flow,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function read_like(param) {
  return request({
    url:path.read_like,
    method: 'post',
    data: {
      ...param
    }
  })
}
export function readmay_like(param) {
  return request({
    url:path.readmay_like,
    method: 'post',
    data: {
      ...param
    }
  })
}
