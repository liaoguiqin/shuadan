import request from '@/utils/request'
const path = {
  'View':'app/input', // 查看
  'lower_level':'app/lower_level', // 获取下线
  'cash_log':'app/cash_log', // 充值明细
  'recharge_order':'app/recharge_order', // 充值记录
  'cancel':'task/cancel', //取消
  'get_task':'task/read', //取消
  'task_add':'task/add', //取消
  'get_goods_info':'task/get_goods_info', //获取商品信息
}

export function get_task(where) {
  console.log(where);
  return request({
    url: path.get_task,
    method: 'get',
    params: where
  })
}
export function cash_log(where) {
  console.log(where);
  return request({
    url: path.cash_log,
    method: 'get',
    params: where
  })
}
export function recharge_order(where) {
  console.log(where);
  return request({
    url: path.recharge_order,
    method: 'get',
    params: where
  })
}

export function get_goods_info(where) {
  console.log(where);
  return request({
    url: path.get_goods_info,
    method: 'get',
    params: where
  })
}
export function cancel(where) {
  return request({
    url: path.cancel,
    method: 'post',
    data:  where
  })
}
export function lower_level(where) {
  return request({
    url: path.lower_level,
    method: 'post',
    data:  where
  })
}
export function task_add(data) {
  let p = JSON.stringify(data);
  let d = JSON.parse(p);
  let base_param =[];
  // 创建新的对象
  let param =  {};
  Object.keys(d).forEach(function(item){
    param[item] = d[item];
  });
  param.field.forEach(function(item,index){
    item['hour'] = item.hour.join(',');
    request({
      url: path.task_add,
      method: 'post',
      data:  Object.assign(param,item)
    }).then(result => {
      console.log(result);
    }).catch(error => {

    })

  });

}

