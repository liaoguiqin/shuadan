import Vue from 'vue'

import 'normalize.css/normalize.css'// A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import App from './App'
import router from './router'
import store from './store'
import  {Message} from 'element-ui'
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
import '@/icons' // icon
import '@/permission' // permission control
import axios from 'axios'
if (!process.env.IS_WEB) Vue.use(require('vue-electron'))


Vue.use(ElementUI,{ zhLocale })
Vue.prototype.$axios=axios
Vue.prototype.Message=Message
Vue.config.productionTip = false

new Vue({
  components: { App },
  router,
  store,

  template: '<App/>'
}).$mount('#app')
