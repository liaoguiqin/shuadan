import Vue from 'vue'
import Router from 'vue-router'
/* Layout */
import Layout from '../views/layout/Layout'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/**
 * hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
 *                                if not set alwaysShow, only more than one route under the children
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noredirect           if `redirect:noredirect` will no redirct in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    title: 'title'               the name show in submenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar,
  }
 **/
  // { path: '/login', component: () => import('@/views/login/index'), hidden: true },
export const constantRouterMap = [
    { path: '/login', component: () => import('@/views/login/index'), hidden: true },
    { path: '/find_pass', component: () => import('@/views/login/find_pass'), hidden: true },
    { path: '/register', component: () => import('@/views/login/register'), hidden: true },
    { path: '/404', component: () => import('@/views/404'), hidden: true },
    { path: '/Recharge', component: () => import('@/views/pay/Recharge'), hidden: true },

    {
      path: '/',
      component: Layout,
      redirect: '/dashboard',
      hidden: true,
      name: '主页',
      children: [{
        path: 'dashboard',
        component: () => import('@/views/dashboard/index'),
        meta: { title: '主页', icon: 'home' }
      },
        {
          path: 'withdraw',
          name: 'withdraw',
          meta: { title: '提现', icon: 'home' },
          component: () => import('@/views/dashboard/withdraw')
        },
        {
          path: 'zixun',
          name: 'zixun',
          meta: { title: '资讯', icon: 'home' },
          component: () => import('@/views/dashboard/zixun')
        }
      ]
    },
    {
      path: '/dashboard',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'dashboard',
          component: () => import('@/views/dashboard/index'),
          meta: { title: '主页', icon: 'home' }
        }
      ]
    },
    {
      path: '/taobao',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'taobao',
          component: () => import('@/views/taobao/index'),
          meta: { title: '淘宝任务', icon: 'taobao' }
        }
      ]
    },

    {
      path: '/jd',
      component: Layout,
      name: 'jd',
      children: [
        {
          path: 'index',
          name: 'jd',
          component: () => import('@/views/jd/index'),
          meta: { title: '京东任务', icon: 'jd' }
        }
      ]
    },

    {
      path: '/pdd',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'pdd',
          component: () => import('@/views/pdd/index'),
          meta: { title: '拼多多任务', icon: 'pdd' }
        }
      ]
    },
    {
      path: '/task',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'task',
          component: () => import('@/views/task/index'),
          meta: { title: '任务查询', icon: 'search' }
        }
      ]
    },
    {
      path: '/ad',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'ad',
          component: () => import('@/views/tg/index'),
          meta: { title: '推广中心', icon: 'tg' }
        }
      ]
    },

    {
      path: '/pay',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'pay',
          component: () => import('@/views/pay/index'),
          meta: { title: '账户充值', icon: 'pay' }
        }
      ]
    },
    {
      path: '/video',
      component: Layout,
      children: [
        {
          path: 'index',
          name: 'video',
          component: () => import('@/views/video/index'),
          meta: { title: '视频教程', icon: 'vd' }
        }
      ]
    },
    { path: '*', redirect: '/404', hidden: true }
  ]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})

