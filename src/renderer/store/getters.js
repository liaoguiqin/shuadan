const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avater: state => state.user.avater,
  nikename: state => state.user.nikename,
  phone: state => state.user.phone,
  all: state => state.user.all,
  currency: state => state.user.currency
}
export default getters
