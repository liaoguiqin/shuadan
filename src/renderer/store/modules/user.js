import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'

const user = {
  state: {
    token: getToken(),
    nickname: '',
    currency: '',
    avatar: '',
    phone: '',
    all:''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_all: (state, all) => {
      state.all = all
    },
    SET_NAME: (state, nikename) => {
      state.nikename = nikename;
    },
    SET_AVATAR: (state, avater) => {
      state.avater = avater
    },
    SET_phone: (state, phone) => {
      state.phone = phone
    },
    SET_currency: (state, currency) => {
      state.currency = currency
    }
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.account.trim()
      return new Promise((resolve, reject) => {
        login(username, userInfo.password).then(response => {
          const data = response.data
          setToken(data.token)
          commit('SET_TOKEN', data.token)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo({ commit, state }) {
      return new Promise((resolve, reject) => {
        getInfo(state.token).then(response => {
          const data = response.data
          if (data) { // 验证返回的roles是否是一个非空数组

          } else {
            reject('getInfo: roles must be a non-null array !')
          }
          commit('SET_NAME', data.nikename)
          commit('SET_AVATAR', data.avater)
          commit('SET_phone', data.phone)
          commit('SET_all', data)
          commit('SET_currency', data.currency)
          resolve(response)

        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_ROLES', [])
          removeToken()
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise(resolve => {
        removeToken()
        commit('SET_TOKEN', '')
        resolve()
      })
    }
  }
}

export default user
